# Tasker

## Installation
First, enter directory on your computer where you want to install and enter next command in terminal:
    git clone https://Programeezy@bitbucket.org/Programeezy/tasktracker.git

Second, you need to install required packages from requirements.txt.
Enter folder with project and type in terminal:

    sudo pip3 install -r requirements.txt
    
Then you can install TaskTracker via command:

    sudo setup.py install

## Quick Start

After you installed tasker, you can get help via command 'tasker -h' in terminal or help(tasker_lib) in interpreter.

### Examples:
Console app:
    
    
    $ tasker task add -n 'Go for a walk' -t 18:00
    $ tasker task show
    > ID: b8296661
    > Name: Go for a walk
    > Description: None
    > Start time - 18:00 | Deadline - None
    > Status - Active
    > Priority - NORMAL
    > User: tim
    > Groups:[]
    > Period: 
    > Subtasks:

    $ tasker task add -n 'Go to store' -t 18:30 -d 20:00 --parent b8296661
    $ tasker task find -n 'Go to store'
    > ID: 06a5cbdb
    > Name: Go to store
    $ tasker task remove 06a5cbdb

Library:
    
    
    >>> from tasker_lib import operations, storage, task
    
    >>>stor = storage.TaskStorage()
    >>> manager = operations.TaskManager(stor)
    >>> new_task = task.Task(user='tim', name='New task', description='One more description') 
    >>> manager.add_task(new_task)
    >>> another_task = task.Task(user='tim', name='Another', priority='high')
    
    #Get task_list formed to task trees(subtasks joined to their parents)
    >>> manager.get_task_trees()
    [<tasker_lib.task.Task object at 0x7fb4d9d1a908>, <tasker_lib.task.Task object at 0x7fb4da453630>]
    
    #Get task_list filtered by different conditions.
    >>> stor.get_tasks_by(priority='high')
    [<tasker_lib.task.Task object at 0x7fb4d81429e8>]
    
    #Add connection between tasks
    >>> manager.add_connection(new_task.id, another_task.id, is_blocking=False)
    
    #Get list of connection tuples in form (first_task, second_task, is_blocking_state)
    >>> connections = manager.get_connected_tasks()
    >>> connection = connections[0]
    >>> connection[0].id == new_task.id and connection[1].id == another_task.id
    True