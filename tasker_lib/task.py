r""" module contains Task model and some enum objects """

import datetime
import dateparser
import uuid

from enum import IntEnum

ACTIVE = 'Active'
DONE = 'Done'
FAILED = 'Failed'

LOW_LABELS = (
    'LOW',
    'Priority.LOW'
)
NORMAL_LABELS = (
    'NORMAL',
    'Priority.NORMAL'
)
HIGH_LABELS = (
    'HIGH',
    'Priority.HIGH'
)


class Priority(IntEnum):
    LOW = 1
    NORMAL = 2
    HIGH = 3

    @staticmethod
    def from_str(label):
        label = label.upper()

        if label in LOW_LABELS:
            return Priority.LOW
        elif label in NORMAL_LABELS:
            return Priority.NORMAL
        elif label in HIGH_LABELS:
            return Priority.HIGH
        else:
            raise NotImplementedError


class Task:
    def __init__(self, user,
                 name='',
                 description='',
                 time_create=None,
                 time_start=None,
                 time_over=None,
                 status=ACTIVE,
                 priority='NORMAL',
                 task_groups=None,
                 id=None,
                 delta_period=None,
                 root_id=None,
                 parent_id=None):
        """
        :param user: str
        :param name: str
        :param description: str
        :param time_create: auto
        :param time_start: str
        :param time_over: str
        :param status: str
        :param priority: task.Priority
        :param task_groups: list()
        :param id: str
        :param delta_period: list or tuple in format[int(days), int(months)]
        :param root_id: auto
        :param parent_id: auto
        """
        self.name = name
        self.description = description

        if time_create is None:
            self.time_create = str(datetime.datetime.now())
        else:
            self.time_create = time_create

        self.time_start = time_start
        self.time_over = time_over

        self.subtasks = []

        if id is None:
            self.id = str(uuid.uuid4())[:8]
        else:
            self.id = id

        self.status = status

        self.priority = Priority.from_str(priority)
        if self.priority is None:
            self.priority = Priority.NORMAL

        if self.time_over:
            if datetime.datetime.now() > dateparser.parse(self.time_over):
                self.failed()

        if task_groups is None:
            self.task_groups = []
        else:
            self.task_groups = task_groups

        self.user = user

        self.delta_period = delta_period

        if root_id is None:
            self.root_id = self.id
        else:
            self.root_id = root_id
        self.parent_id = parent_id

    def add_subtask(self, task):
        task.parent_id = self.id
        task.root_id = self.root_id
        self.subtasks.append(task)

    def done(self):
        if self.time_over:
            if self.time_over < datetime.datetime.now():
                return
            
        self.status = DONE

    def failed(self):
        self.status = FAILED


class Connection:
    def __init__(self, first_task_id, second_task_id, is_blocking=False):
        self.first_task_id = first_task_id
        self.second_task_id = second_task_id
        self.is_blocking = is_blocking


class TaskGroup:
    def __init__(self, name):
        self.name = name
        self.tasks = []
