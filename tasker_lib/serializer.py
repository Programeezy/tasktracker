r"""Module for serialize and deserialize json arrays"""

import json
import logging
from tasker_lib.storage import FIELD_PRIORITY

FIELD_FIRST_TASK_ID = 'first_task_id'
FIELD_SECOND_TASK_ID = 'second_task_id'
FIELD_IS_BLOCKING = 'is_blocking'

SERIALIZE_FIELDS = (
    'user',
    'name',
    'description',
    'time_create',
    'time_start',
    'time_over',
    'status',
    FIELD_PRIORITY,
    'task_groups',
    'id',
    'delta_period',
    'root_id',
    'parent_id'
)


def load_json(path):
    try:
        with open(path, 'r') as file:
            json_arr = []

            for data in json.load(file):
                json_arr.append(data)
            return json_arr

    except json.JSONDecodeError:
        return []
    except FileNotFoundError:
        with open(path, 'w'):
            pass
        return []
    except Exception as e:
        logging.error('Error reading a json file. ' + str(e))


def save_json(path, json_dict):
    with open(path, 'w') as file:
        json.dump(json_dict, file, indent=4)


def push_task(path, task):
    arr_json = load_json(path)
    arr_json.append(make_dict(task))
    save_json(path, arr_json)


def push_task_list(path, task_list):
    # Дозаписывает список задач в json файл
    arr_json = load_json(path)
    dict_list = [make_dict(item) for item in task_list]
    dict_list += arr_json
    save_json(path, dict_list)


def save_task_list(path, task_list):
    dict_list = [make_dict(item) for item in task_list]
    save_json(path, dict_list)


def make_dict(task):
    task_dict = {k: v for k, v in task.__dict__.items() if k in SERIALIZE_FIELDS}
    task_dict[FIELD_PRIORITY] = str(task_dict[FIELD_PRIORITY])
    return task_dict


def conn_to_dict(connection):
    conn_dict = {
        FIELD_FIRST_TASK_ID: connection.first_task_id,
        FIELD_SECOND_TASK_ID: connection.second_task_id,
        FIELD_IS_BLOCKING: connection.is_blocking
    }
    return conn_dict
