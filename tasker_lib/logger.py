r"""
Module with logger setup.
"""

import logging

LOGGER_NAME = "Tasker"


def get_logger():
    return logging.getLogger(LOGGER_NAME)


def enable_logger(enabled=True):
    logger = logging.getLogger(LOGGER_NAME)
    logger.disabled = not enabled
    return logger


def set_level_logger(level):
    level = logging.getLevelName(level)
    logger = logging.getLogger(LOGGER_NAME)
    logger.setLevel(level)
