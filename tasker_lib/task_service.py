r"""
Module with additional functions to handle task lists
"""

import datetime
import re

import dateparser
from tasker_lib.task import Task

PRIORITY = 'priority'
SUBTASKS = 'subtasks'


def find_task(task_list, task_id):
    for task in task_list:
        if task.id == task_id:
            return task
        for sub in task.subtasks:
            subtask = find_sub(sub, task_id)
            if subtask:
                return subtask
    return None


def find_sub(task, task_id):
    if task.id == task_id:
        return task
    for sub in task.subtasks:
        subtask = find_sub(sub, task_id)
        if subtask:
            return subtask
    return None


def compose_tree(tree_list):
    for task_item in tree_list:
        if task_item.parent_id:
            parent_task = find_task(tree_list, task_item.parent_id)
            if parent_task:
                tree_list.remove(task_item)
                parent_task.subtasks.append(task_item)

        if task_item.delta_period:
            prev_time = dateparser.parse(task_item.time_start)
            if prev_time < datetime.datetime.now():
                task_dict = task_item.__dict__.copy()
                subtasks = task_dict.pop(SUBTASKS)
                task_dict[PRIORITY] = str(task_dict[PRIORITY])
                new_task = Task(**task_dict)
                new_task.subtasks = subtasks
                delta_dict = delta_parse(task_item.delta_period)
                # Sum old date with delta of regular task
                delta_dict.update((key, getattr(prev_time, key) + value) for key, value in delta_dict.items())
                new_task.time_start = str(prev_time.replace(**delta_dict))
                tree_list.append(new_task)
    return tree_list


def delta_parse(date):
    """
    :param date: string in format '%d'y'%d'm'%d'd'%d'h in any order
    :return: dictionary of date to replace
    """
    pattern = re.compile(r'(\d+[ymdh])')
    res = re.findall(pattern, date)
    delta_dict = {}
    for word in res:
        if word[-1] == 'y':
            delta_dict.update({'year': int(word[:-1])})
        if word[-1] == 'm':
            delta_dict.update({'month': int(word[:-1])})
        if word[-1] == 'd':
            delta_dict.update({'day': int(word[:-1])})
        if word[-1] == 'h':
            delta_dict.update({'hour': int(word[:-1])})
    return delta_dict


def sort_tasks_by_priority(task_list):
    task_list.sort(key=lambda x: x.priority, reverse=True)

