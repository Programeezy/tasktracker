r""" Library for creating and controlling tasks lists.
All high-level operations are located in :mod: `operations`.

Modules of package:
logger.py - module for setup logging in lib
operations.py - module contains class TaskManager with high-level API methods
serializer.py - module contains functions  to serialize and deserialize models of lib
storage.py - module for collecting, filtering etc models of lib
task.py - contains models of lib
task_service.py - contains additional functions

Quick start:

>>> from tasker_lib import operations, storage, task

>>> stor = storage.TaskStorage()
>>> manager = operations.TaskManager(stor)
>>> new_task = task.Task(user='tim', name='New task', description='One more description')
>>> manager.add_task(new_task)
>>> another_task = task.Task(user='tim', name='Another', priority='high')

#Get task_list formed to task trees(subtasks joined to their parents)
>>> manager.get_task_trees()
[<tasker_lib.task.Task object at 0x7fb4d9d1a908>, <tasker_lib.task.Task object at 0x7fb4da453630>]

#Get task_list filtered by different conditions.
>>> stor.get_tasks_by(priority='high')
[<tasker_lib.task.Task object at 0x7fb4d81429e8>]

#Add connection between tasks
>>> manager.add_connection(new_task.id, another_task.id, is_blocking=False)

#Get list of connection tuples in form (first_task, second_task, is_blocking_state)
>>> connections = manager.get_connected_tasks()
>>> connection = connections[0]
>>> connection[0].id == new_task.id and connection[1].id == another_task.id
True

"""