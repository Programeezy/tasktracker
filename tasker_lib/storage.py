r""" Contains class for store and edit task list
"""

from tasker_lib import (
    serializer,
    task
)

FIELD_ID = 'id'
FIELD_USER = 'user'
FIELD_NAME = 'name'
FIELD_TIME_START = 'time_start'
FIELD_TIME_OVER = 'time_over'
FIELD_TIME_CREATE = 'time_create'
FIELD_PRIORITY = 'priority'
FIELD_STATUS = 'status'
FIELD_TASK_GROUPS = 'task_groups'
FIELD_FIRST_TASK_ID = 'first_task_id'
FIELD_PARENT_ID = 'parent_id'
FIELD_ROOT_ID = 'root_id'
DEFAULT_TASKS_FILENAME = 'tasks.json'
DEFAULT_CONNECTIONS_FILENAME = 'connections.json'
DEFAULT_GROUPS_FILENAME = 'groups.json'


class TaskStorage:
    def __init__(self, path=DEFAULT_TASKS_FILENAME, conn_path=DEFAULT_CONNECTIONS_FILENAME, group_path=DEFAULT_GROUPS_FILENAME):
        self.path = path
        self.conn_path = conn_path
        self.group_path = group_path

    def get_tasks(self):
        task_list_json_arr = serializer.load_json(self.path)
        task_list = []
        for task_dict in task_list_json_arr:
            task_list.append(task.Task(**task_dict))
        return task_list

    def get_connections(self):
        conn_arr_json = serializer.load_json(self.conn_path)
        conn_arr = []
        for item in conn_arr_json:
            conn_arr.append(task.Connection(**item))
        return conn_arr

    def get_connection_by_id(self, task_id):
        conn_arr_json = serializer.load_json(self.conn_path)

        for conn_dict in conn_arr_json:
            if conn_dict[FIELD_FIRST_TASK_ID] == task_id:
                return task.Connection(**conn_dict)

        return None

    def get_group_names(self):
        return serializer.load_json(self.group_path)

    def get_task_group(self, group_name, username):
        group_arr = serializer.load_json(self.group_path)

        if group_name in group_arr:
            group = task.TaskGroup(group_name)
            group.tasks = self.get_tasks_by(group=group_name, user=username)
            return group
        else:
            return None

    def get_tasks_by_user(self, username):
        task_list_json_arr = serializer.load_json(self.path)
        task_list = []
        for task_dict in task_list_json_arr:
            new_task = task.Task(**task_dict)
            if new_task.user == username:
                task_list.append(new_task)
        return task_list

    def get_tasks_by(
            self,
            user=None,
            name=None,
            time_start=None,
            time_over=None,
            time_create=None,
            priority=None,
            group=None,
            status=None
    ):
        json_arr = serializer.load_json(self.path)
        filtered_arr = []

        if user:
            filtered_arr = [x for x in json_arr if x[FIELD_USER] == user]
        if name:
            filtered_arr = [x for x in json_arr if x[FIELD_NAME] == name]

        if time_start:
            filtered_arr = [x for x in json_arr if x[FIELD_TIME_START] == time_start]

        if time_over:
            filtered_arr = [x for x in json_arr if x[FIELD_TIME_OVER] == time_over]

        if time_create:
            filtered_arr = [x for x in json_arr if x[FIELD_TIME_CREATE] == time_create]

        if priority:
            filtered_arr = [x for x in json_arr
                            if task.Priority.from_str(x[FIELD_PRIORITY]) == task.Priority.from_str(priority)]

        if group:
            filtered_arr = [x for x in json_arr if group in x[FIELD_TASK_GROUPS]]

        if status:
            filtered_arr = [x for x in json_arr if x[FIELD_STATUS] == status]
        filtered_list = []

        for task_dict in filtered_arr:
            filtered_list.append(task.Task(**task_dict))

        return filtered_list

    def get_task_by_id(self, task_id):
        task_list_json_arr = serializer.load_json(self.path)
        for task_dict in task_list_json_arr:
            if task_dict[FIELD_ID] == task_id:
                return task.Task(**task_dict)
        return None

    def get_tasks_by_id(self, task_id):
        task_list_json_arr = serializer.load_json(self.path)
        task_list = []
        for task_dict in task_list_json_arr:
            new_task = task.Task(**task_dict)
            if new_task.id == task_id or new_task.parent_id == task_id or new_task.root_id == task_id:
                task_list.append(new_task)
        return task_list

    def get_tasks_by_id_list(self, task_id_list):
        json_arr = serializer.load_json(self.path)
        task_list = []

        for task_dict in json_arr:
            if task_dict[FIELD_ID] in task_id_list:
                task_list.append(task.Task(**task_dict))

        return task_list

    def remove_task(self, task_id):
        task_json_arr = serializer.load_json(self.path)
        for task_dict in task_json_arr:
            if task_dict[FIELD_ID] == task_id:
                task_json_arr.remove(task_dict)

        serializer.save_json(self.path, task_json_arr)

    def remove_task_tree(self, task_id):
        task_list_json_arr = serializer.load_json(self.path)
        new_list = [x for x in task_list_json_arr if not (x[FIELD_ID] == task_id
                                                          or x[FIELD_PARENT_ID] == task_id
                                                          or x[FIELD_ROOT_ID] == task_id)]

        serializer.save_json(self.path, new_list)

    def save_task_tree(self, task_tree):
        task_json_arr = []

        for task_item in task_tree:
            task_dict = serializer.make_dict(task_item)
            task_json_arr.append(task_dict)

        serializer.save_json(self.path, task_json_arr)

    def save_connections(self, connections_array):
        json_arr = []
        for conn in connections_array:
            json_arr.append(serializer.conn_to_dict(conn))
        serializer.save_json(self.conn_path, json_arr)

    def update_connections(self, conn):
        conn_arr = self.get_connections()
        conn_arr.append(conn)
        self.save_connections(conn_arr)

    def update(self, updated_task):
        task_json_arr = serializer.load_json(self.path)
        group_arr = serializer.load_json(self.group_path)
        new_task_dict = serializer.make_dict(updated_task)
        group_arr = set(group_arr)
        group_arr.update(updated_task.task_groups)
        serializer.save_json(self.group_path, list(group_arr))

        for task_dict in task_json_arr:
            if task_dict[FIELD_ID] == updated_task.id:
                task_json_arr.remove(task_dict)
                task_json_arr.append(new_task_dict)
                serializer.save_json(self.path, task_json_arr)
                return
        task_json_arr.append(new_task_dict)
        serializer.save_json(self.path, task_json_arr)
