r"""
Module for basic operations with TaskStorage
"""

import dateparser

from tasker_lib.storage import (
    DEFAULT_TASKS_FILENAME,
    DEFAULT_CONNECTIONS_FILENAME,
    DEFAULT_GROUPS_FILENAME,
    TaskStorage
)
from tasker_lib import task
from tasker_lib.logger import (
    get_logger
)
from tasker_lib.task_service import (
    compose_tree,
    find_task,
)


class TaskManager:
    def __init__(self, storage=None, storage_path=''):
        if storage is None:
            self.storage = TaskStorage(storage_path + DEFAULT_TASKS_FILENAME,
                                       storage_path + DEFAULT_CONNECTIONS_FILENAME,
                                       storage_path + DEFAULT_GROUPS_FILENAME
                                       )
        else:
            self.storage = storage

    def get_task_group(self, group_name, username=None):
        """
        :param group_name: Name of group to get
        :param username: filter by username
        :return: TaskGroup object if exists otherwise None
        """
        group = self.storage.get_task_group(group_name, username)
        return group

    def add_task(self, task):
        if task.parent_id:
            parent = self.storage.get_task_by_id(task.parent_id)
            if parent:
                task.root_id = parent.root_id
            else:
                task.parent_id = None

        self.storage.update(task)
        get_logger().info(task.name + ' added')

    def add_connection(self, first_id, second_id, is_blocking=False):
        first = self.storage.get_task_by_id(first_id)
        second = self.storage.get_task_by_id(second_id)

        if first and second:
            get_logger().info('Tasks ' + first_id + ' and ' + second_id + ' are found')
            self.storage.update_connections(task.Connection(first_id, second_id, is_blocking))
        else:
            get_logger().error('Tasks ' + first_id + ' and ' + second_id + ' not found')

    def remove_connection(self, first_id, second_id):
        connections = self.storage.get_connections()

        connections = (lambda x: (x.first_task_id == first_id and x.second_id == second_id) or
                                 (x.first_task_id == second_id and x.second_id == first_id), connections)
        self.storage.save_connections(connections)

    def remove_task(self, task_id):
        self.storage.remove_task(task_id)
        get_logger().info('Task has been removed')

    def get_connected_tasks(self):
        """
        Collect all tasks with connection
        :return: List of tuples in format (first_task, second_task, boolean(is_blocking))
        """
        connections = self.storage.get_connections()
        id_set = set()

        for conn in connections:
            id_set.add(conn.first_task_id)
            id_set.add(conn.second_task_id)

        task_list = self.storage.get_tasks_by_id_list(list(id_set))
        connected_tasks_tuples = []

        for conn in connections:
            first_task = find_task(task_list, conn.first_task_id)
            second_task = find_task(task_list, conn.second_task_id)
            is_blocking = conn.is_blocking
            connected_tasks_tuples.append((first_task, second_task, is_blocking))

        return connected_tasks_tuples

    def get_task_tree_by_id(self, task_id):
        tree_list = self.storage.get_tasks_by_id(task_id)
        return compose_tree(tree_list)[0]

    def get_task_trees(self, username=None):
        r"""
        :param username: enter username to get tasks only of user with this username
        :return: tree of tasks in storage
        """
        if username is None:
            get_logger().info('Username is not set. Getting all tasks in database...')
            task_list = self.storage.get_tasks()
        else:
            get_logger().error('Getting tasks for user ' + username)
            task_list = self.storage.get_tasks_by_user(username)

        return compose_tree(task_list)

    def make_done(self, task_id):
        r"""
        Mark task as done
        :param task_id: id of task to work with
        """

        founded = self.get_task_tree_by_id(task_id)
        if founded:
            conn = self.storage.get_connection_by_id(task_id)

            if conn:
                if conn.is_blocking:
                    blocking_task = self.storage.get_task_by_id(conn.second_task_id)
                    if blocking_task.status != task.DONE:
                        get_logger().info('Blocking task is not done')
                        return
            founded.done()
            get_logger().info('Task ' + task_id + ' is done')
            self.storage.update(founded)
        else:
            get_logger().error('Task ' + task_id + ' not found')

    def edit_task(self,
                  task_id,
                  name=None,
                  description=None,
                  time_start=None,
                  time_over=None,
                  priority=None,
                  delta_period=None,
                  ):
        task = self.storage.get_task_by_id(task_id)

        if task is None:
            get_logger().error('Wrong task ID: ' + task_id)
            return

        if name:
            task.name = name

        if description:
            task.description = description

        if priority:
            if priority in [task.priority.NORMAL, task.priority.LOW, task.priority.HIGH]:
                task.priority = priority

        if time_start:
            if dateparser.parse(time_start):
                task.time_start = time_start

        if time_over:
            if dateparser.parse(time_over):
                task.time_over = time_over

        if delta_period:
            task.delta_period = delta_period

        self.storage.update(task)
