import datetime
import os
import unittest

from tasker_lib.operations import TaskManager
from tasker_lib import storage
from tasker_lib import task
from tasker_lib.task_service import sort_tasks_by_priority

STORAGE_PATH = 'test_tasks.json'
CONN_PATH = 'connections.json'
GROUPS_PATH = 'groups.json'
USERNAME = 'test'
TASK_NAME_ONE = 'test_name'
TASK_NAME_TWO = 'second_test_name'
GROUP_ONE = 'Old'
GROUP_TWO = 'New'


class TestOperations(unittest.TestCase):
    def setUp(self):
        self.task_storage = storage.TaskStorage(path=STORAGE_PATH)
        self.manager = TaskManager(self.task_storage)
        self.test_task = task.Task(
            user=USERNAME,
            name=TASK_NAME_ONE,
            priority='NORMAL',
            task_groups=[GROUP_ONE]
        )
        self.test_task_two = task.Task(
            user=USERNAME,
            name=TASK_NAME_TWO,
            priority='HIGH',
            task_groups=[GROUP_TWO]
        )

    def test_add_task(self):
        task_list = self.task_storage.get_tasks()
        old_len = len(task_list)
        self.manager.add_task(self.test_task)
        task_list = self.task_storage.get_tasks()
        new_len = len(task_list)
        self.assertGreater(new_len, old_len)

    def test_get_task_tree(self):
        self.manager.add_task(self.test_task)
        task_tree = self.manager.get_task_trees()
        self.assertGreater(len(task_tree), 0)

    def test_add_regular_task(self):
        self.test_task.delta_period = '3d'
        time_start = datetime.datetime.now() - datetime.timedelta(days=2)
        self.test_task.time_start = str(time_start)
        self.manager.add_task(self.test_task)
        length = len(self.manager.get_task_trees())
        self.assertEqual(2, length)

    def test_add_subtask(self):
        test_subtask = task.Task(user=USERNAME, name=TASK_NAME_TWO, parent_id=self.test_task.id)
        self.manager.add_task(self.test_task)
        self.manager.add_task(test_subtask)
        task_tree = self.manager.get_task_trees()
        subtasks_len = len(task_tree[0].subtasks)
        self.assertGreater(subtasks_len, 0)

    def test_remove_task(self):
        self.manager.add_task(self.test_task)
        old_len = self.manager.get_task_trees()
        self.manager.remove_task(self.test_task.id)
        new_len = self.manager.get_task_trees()
        self.assertGreater(old_len, new_len)

    def test_add_connection(self):
        self.manager.add_task(self.test_task)
        self.manager.add_task(self.test_task_two)
        self.manager.add_connection(self.test_task.id, self.test_task_two.id)
        connection_tuple = self.manager.get_connected_tasks()[0]
        self.assertEqual(self.test_task.id, connection_tuple[0].id)
        self.assertEqual(self.test_task_two.id, connection_tuple[1].id)

    def test_block_connection(self):
        self.manager.add_task(self.test_task)
        self.manager.add_task(self.test_task_two)
        self.manager.add_connection(self.test_task.id, self.test_task_two.id, is_blocking=True)
        self.manager.make_done(self.test_task.id)
        task_status = self.manager.get_task_tree_by_id(self.test_task.id).status
        self.assertNotEqual(task.DONE, task_status)

    def test_edit_task(self):
        self.manager.add_task(self.test_task)
        self.manager.edit_task(self.test_task.id, name='test_edit')
        tasks_tree = self.manager.get_task_tree_by_id(self.test_task.id)
        self.assertEqual(tasks_tree.name, 'test_edit')

    def test_done_task(self):
        self.manager.add_task(self.test_task)
        self.manager.make_done(self.test_task.id)
        task_tree = self.manager.get_task_tree_by_id(self.test_task.id)
        self.assertEqual(task_tree.status, task.DONE)

    def test_tree_by_id(self):
        self.manager.add_task(self.test_task)
        self.manager.add_task(task.Task(user='test', name='new_task'))
        tree = self.manager.get_task_trees()
        self.assertEqual(len(tree), 2)
        tree_by_id = self.manager.get_task_tree_by_id(self.test_task.id)
        self.assertEqual(tree_by_id.id, self.test_task.id)

    def test_priority_sort(self):
        self.manager.add_task(self.test_task)
        self.manager.add_task(self.test_task_two)
        task_list = self.manager.get_task_trees()
        sort_tasks_by_priority(task_list)
        self.assertEqual(self.test_task_two.id, task_list[0].id)
        self.assertEqual(self.test_task.id, task_list[1].id)

    def test_groups(self):
        self.manager.add_task(self.test_task)
        self.manager.add_task(self.test_task_two)
        group_one = self.manager.get_task_group(GROUP_ONE)
        group_two = self.manager.get_task_group(GROUP_TWO)
        empty_group = self.manager.get_task_group('random')
        self.assertEqual(1, len(group_one.tasks))
        self.assertEqual(1, len(group_two.tasks))
        self.assertEqual(None, empty_group)

    def tearDown(self):
        if os.path.exists(STORAGE_PATH):
            os.remove(STORAGE_PATH)
        if os.path.exists(CONN_PATH):
            os.remove(CONN_PATH)
        if os.path.exists(GROUPS_PATH):
            os.remove(GROUPS_PATH)