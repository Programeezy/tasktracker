import os
import unittest

from tasker_lib import operations
from tasker_lib import storage
from tasker_lib import task

STORAGE_PATH = 'test_storage.json'
GROUPS_PATH = 'groups.json'
USERNAME = 'test'
USERNAME_TEST = 'changed_test'
TASK_NAME_ONE = 'test_name'
TASK_NAME_TWO = 'test_name_two'


class TestStorage(unittest.TestCase):
    def setUp(self):
        self.storage = storage.TaskStorage(STORAGE_PATH)
        self.task = task.Task(user=USERNAME, name=TASK_NAME_ONE)
        self.subtask = task.Task(user=USERNAME_TEST, name=TASK_NAME_TWO, parent_id=self.task.id)

    def test_update(self):
        self.storage.update(self.task)
        length = len(self.storage.get_tasks())
        self.assertEqual(length, 1)
        self.task.name = 'changed_test_name'
        self.storage.update(self.task)
        new_length = len(self.storage.get_tasks())
        self.assertEqual(new_length, 1)

    def test_get_tasks(self):
        old_len = len(self.storage.get_tasks())
        self.assertEqual(old_len, 0)
        self.storage.update(self.task)
        new_len = len(self.storage.get_tasks())
        self.assertEqual(new_len, 1)

    def test_get_tasks_by_user(self):
        self.storage.update(self.task)
        self.storage.update(self.subtask)
        len_tasks = len(self.storage.get_tasks())
        self.assertEqual(len_tasks, 2)
        len_tasks_by_user = len(self.storage.get_tasks_by_user(username=USERNAME))
        self.assertEqual(len_tasks_by_user, 1)

    def test_get_tasks_by_id(self):
        self.storage.update(self.task)
        manager = operations.TaskManager(self.storage)
        manager.add_task(self.subtask)
        task_list = self.storage.get_tasks_by_id(self.task.id)
        self.assertEqual(len(task_list), 2)

    def test_remove_task(self):
        self.storage.update(self.task)
        old_len = len(self.storage.get_tasks())
        self.assertEqual(old_len, 1)
        self.storage.remove_task(self.task.id)
        new_len = len(self.storage.get_tasks())
        self.assertEqual(new_len, 0)

    def test_remove_task_tree(self):
        self.storage.update(self.task)
        task_manager = operations.TaskManager(self.storage)
        task_manager.add_task(self.subtask)
        old_len = len(self.storage.get_tasks())
        self.assertEqual(old_len, 2)
        self.storage.remove_task_tree(self.task.id)
        length = len(self.storage.get_tasks())
        self.assertEqual(length, 0)

    def tearDown(self):
        if os.path.exists(STORAGE_PATH):
            os.remove(STORAGE_PATH)
        if os.path.exists(GROUPS_PATH):
            os.remove(GROUPS_PATH)
