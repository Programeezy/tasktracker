import datetime
import os
import subprocess
import unittest


from tasker_lib import storage, task
from tasker_lib import operations

APP_NAME = 'tasker'
STORAGE_PATH = 'tasks.json'
CONN_PATH = 'connections.json'


class TestConsoleApp(unittest.TestCase):
    def setUp(self):
        self.storage = storage.TaskStorage(STORAGE_PATH)
        self.manager = operations.TaskManager(self.storage)
        self.task = task.Task(user='test', name='test')
        self.second_task = task.Task(user='test', name='test_two')
        self.subtask = task.Task(user='test', name='subtask_test', parent_id=self.task.id)

    def test_task_add(self):
        arg_line = [APP_NAME, '--storage', STORAGE_PATH, 'task', 'add', '-n', 'test_task']
        subprocess.call(arg_line)
        length = len(self.manager.get_task_trees())
        self.assertEqual(1, length)

    def test_task_remove(self):
        self.manager.add_task(self.task)
        old_len = len(self.manager.get_task_trees())
        self.assertEqual(1, old_len)
        arg_line = [APP_NAME, '--storage', STORAGE_PATH, 'task', 'remove', self.task.id]
        subprocess.call(arg_line)
        new_len = len(self.manager.get_task_trees())
        self.assertEqual(0, new_len)

    def test_subtask_add(self):
        self.manager.add_task(self.task)
        arg_line = [APP_NAME, '--storage', STORAGE_PATH, 'task', 'add', '--parent', self.task.id, '-n', 'test_subtask']
        subprocess.call(arg_line)
        subtasks_len = len(self.manager.get_task_trees()[0].subtasks)
        self.assertEqual(1, subtasks_len)

    def test_subtask_remove(self):
        self.manager.add_task(self.task)
        self.manager.add_task(self.subtask)
        old_len = len(self.manager.get_task_trees()[0].subtasks)
        self.assertEqual(1, old_len)
        arg_line = [APP_NAME, '--storage', STORAGE_PATH, 'task', 'remove', self.subtask.id]
        subprocess.call(arg_line)
        new_len = len(self.manager.get_task_trees()[0].subtasks)
        self.assertEqual(0, new_len)

    def test_task_add_regular(self):
        time = datetime.datetime.now() - datetime.timedelta(days=0)
        arg_line = [APP_NAME, '--storage', STORAGE_PATH, 'task', 'add', '-n', 'test_task', '-r', '2d', '-t', str(time)]
        subprocess.call(arg_line)
        length = len(self.manager.get_task_trees())
        self.assertEqual(2, length)

    def test_connection_add(self):
        self.manager.add_task(self.task)
        self.manager.add_task(self.second_task)
        arg_line = [APP_NAME, '--storage', STORAGE_PATH, 'connection', 'add', self.task.id, self.second_task.id]
        subprocess.call(arg_line)
        connection_tuple = self.manager.get_connected_tasks()[0]
        self.assertEqual(self.task.id, connection_tuple[0].id)
        self.assertEqual(self.second_task.id, connection_tuple[1].id)

    def tearDown(self):
        if os.path.exists(STORAGE_PATH):
            os.remove(STORAGE_PATH)
        if os.path.exists(CONN_PATH):
            os.remove(CONN_PATH)
        if os.path.exists('config.ini'):
            os.remove('config.ini')
