r"""Console application for demonstrating functionality of tasker_lib.

Quick Start:

$ tasker task add -n 'Go for a walk' -t 18:00
$ tasker task show
> ID: b8296661
> Name: Go for a walk
> Description: None
> Start time - 18:00 | Deadline - None
> Status - Active
> Priority - NORMAL
> User: tim
> Groups:[]
> Period:
> Subtasks:

$ tasker task add -n 'Go to store' -t 18:30 -d 20:00 --parent b8296661
$ tasker task find -n 'Go to store'
> ID: 06a5cbdb
> Name: Go to store
$ tasker task remove 06a5cbdb

"""