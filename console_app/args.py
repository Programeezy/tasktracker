r"""Module with generating and parsing command-line args"""

# PYTHON_ARGCOMPLETE_OK

import argparse
import argcomplete


def parse(list_id):
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--storage', action='store', help='Path to db file with tasks')
    parser.add_argument('-l', '--logging', action='store', choices=['DEBUG', 'INFO', 'ERROR'], help='Set logging level')
    operation_types = parser.add_subparsers(dest='cmd')
    task_parser_commands = operation_types.add_parser('task', help='Task commands')
    task_parser = task_parser_commands.add_subparsers(dest='task_oper')
    connection_parser_commands = operation_types.add_parser('connection', help='Connection commands')
    connection_parser = connection_parser_commands.add_subparsers(dest='connection_oper')

    # Task commands
    add_parser = task_parser.add_parser('add', help='Type task info to add task')
    add_args(add_parser)
    remove_parser = task_parser.add_parser('remove', help='Remove task from your task list')
    remove_parser.add_argument('id', choices=list_id, help='Id of task you want to remove')
    edit_parser = task_parser.add_parser('edit', help='Edit task in your task list')
    edit_parser.add_argument('id', choices=list_id)
    edit_parser.add_argument('field')
    edit_parser.add_argument('value')
    done_parser = task_parser.add_parser('done', help='Mark your task as done')
    done_parser.add_argument('id', choices=list_id)
    find_parser = task_parser.add_parser('find', help='Find task with specific fields')
    find_parser.add_argument('-n', '--name', action='store', help='Name of tasks')
    find_parser.add_argument('-p',
                             '--priority',
                             action='store',
                             choices=['LOW', 'NORMAL', 'HIGH'],
                             default=None, help='Priority of tasks')
    find_parser.add_argument('-s',
                             '--status',
                             action='store',
                             choices=['LOW', 'NORMAL', 'HIGH'],
                             default=None,
                             help='Status of tasks')
    find_parser.add_argument('-g', '--group',  action='store', default=None, help='Group of tasks')
    find_parser.add_argument('-c', '--create', action='store', default=None, help='Time of creating of tasks')
    find_parser.add_argument('-t', '--time_start', action='store', metavar='t', default=None, help='Time of tasks')
    find_parser.add_argument('-d', '--deadline', action='store', metavar='d', default=None, help='Deadline of tasks')
    find_parser.add_argument('--id', action='store', help='Id of task', choices=list_id)
    show_parser = task_parser.add_parser('show', help='Show full information of selected tasks')
    show_parser.add_argument('choice', choices=['active', 'all', 'connections'], nargs='?', default=None)
    show_parser.add_argument('--id', action='store', choices=list_id, help='Id of task')
    show_parser.add_argument('-n', '--name', action='store', help='Name of tasks')
    show_parser.add_argument('-p', '--priority', action='store', default=None, help='Priority of tasks')
    show_parser.add_argument('-g', '--group', action='store', default=None, help='Group of tasks')
    show_parser.add_argument('-s', '--status', action='store', default=None, help='Status of tasks')
    # Connection commands
    add_connection_parser = connection_parser.add_parser('add', help='Add connection between two tasks')
    add_connection_parser.add_argument('id', choices=list_id)
    add_connection_parser.add_argument('second_id', choices=list_id)
    add_connection_parser.add_argument('-b',
                                       '--blocking',
                                       action='store_true',
                                       default=False,
                                       help='Marks connection as blocking'
                                       )
    remove_connection_parser = connection_parser.add_parser('remove', help='Remove connection between two tasks')
    remove_connection_parser.add_argument('id', choices=list_id)
    remove_connection_parser.add_argument('second_id', choices=list_id)
    find_connection_parser = connection_parser.add_parser('find', help='Find all connections of task with specified id')
    find_connection_parser.add_argument('id', choices=list_id)
    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    return args


def add_args(parser):
    parser.add_argument('-n', '--name', action='store', help='Name of task you want to add')
    parser.add_argument('--description', action='store', default=None)
    parser.add_argument('-p', '--priority', action='store', choices=['LOW', 'NORMAL', 'HIGH'], default='NORMAL')
    parser.add_argument('-g', '--group', nargs='+', default=[])
    parser.add_argument('-r', '--regular', action='store', default='')
    parser.add_argument('-t', '--time_start', action='store', metavar='t', default=None)
    parser.add_argument('-d', '--deadline', action='store', metavar='d', default=None)
    parser.add_argument('-i', '--interactive', action='store_true')
    parser.add_argument('--parent', action='store', default=None)
