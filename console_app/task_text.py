def show_task(task, subtask_level=0, indent=2):
    print()
    print('  ' * indent * subtask_level + '>', 'ID: ' + task.id)
    print('  ' * indent * subtask_level + '>', 'Name: ' + task.name)
    print('  ' * indent * subtask_level + '>', 'Description: ' + str(task.description))
    print('  ' * indent * subtask_level + '>', 'Start time - ' + str(task.time_start), end='')
    print(' | Deadline - ' + str(task.time_over))
    print('  ' * indent * subtask_level + '>', 'Status - ' + task.status)
    print('  ' * indent * subtask_level + '>', 'Priority - ' + str(task.priority)[9:])
    print('  ' * indent * subtask_level + '>', 'User: ' + task.user)
    print('  ' * indent * subtask_level + '>', 'Groups:' + str(task.task_groups))
    print('  ' * indent * subtask_level + '>', 'Period: ' + str(task.delta_period))
    print('  ' * indent * subtask_level + '>', 'Subtasks:')
    if task.subtasks is None:
        print('  ' * indent * subtask_level + '>', 'None')
        return
    for subtask in task.subtasks:
        show_task(subtask, subtask_level + 1)


def show_task_short(task):
    print('>', 'ID:', task.id)
    print('>', 'Name:', task.name)


def show_connection(conn):
    show_task_short(conn[0])
    print()
    print('> Connected to: ')
    print()
    show_task_short(conn[1])
    if conn[2]:
        print('> with blocking')


def show_connections(connections):
    for conn in connections:
        show_connection(conn)
