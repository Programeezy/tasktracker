r"""
Module for read data from config file
"""

import configparser


class Config:
    def __init__(self, path='config.ini'):
        self.config = configparser.ConfigParser()
        self.path = path
        self.config.read(path)

    def get_path(self):
        path = self.config.get('DEFAULT', 'Path', fallback='tasks.json')
        self.config['DEFAULT']['Path'] = path
        with open(self.path, 'w') as configfile:
            self.config.write(configfile)
        return path

    def get_logging(self):
        logging = self.config.get('DEFAULT', 'Logging', fallback=True)
        self.config['DEFAULT']['Logging'] = logging
        with open(self.path, 'w') as configfile:
            self.config.write(configfile)
        return logging
