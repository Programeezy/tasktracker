#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK

r"""Console application for tasker_lib"""

import getpass

from console_app import (
    args,
    config
)
from console_app.task_text import (
    show_task,
    show_task_short,
    show_connection,
    show_connections
)
from tasker_lib import (
    operations,
    storage,
    task,
    logger
)


def get_all_id(task_list):

    def find_id(id_list, current):
        id_list.append(current.id)

        for sub in current.subtasks:
            find_id(id_list, sub)

    id_list = []
    [find_id(id_list, tsk) for tsk in task_list]
    return id_list


def get_all_groups(task_list):
    groups = set()
    for tsk in task_list:
        for gr in tsk.task_groups:
            groups.add(gr)
    return list(groups)


def print_tasks(task_list):
    for tsk in task_list:
        show_task(tsk)


def create_task(user):
    name = input('Enter task name: ')
    description = input('Enter task description: ')
    time = input('Task start time: ')
    deadline = input('Task deadline: ')
    new_task = task.Task(user=user, name=name, description=description, time_start=time, time_over=deadline)
    return new_task


def main():
    conf = config.Config('config.ini')
    task_storage = storage.TaskStorage(conf.get_path())
    task_manager = operations.TaskManager(task_storage)
    tasks = task_manager.get_task_trees()
    id_choices = get_all_id(tasks)
    arguments = args.parse(id_choices)

    if arguments.storage:
        task_storage = storage.TaskStorage(arguments.storage)
        task_manager = operations.TaskManager(task_storage)

    if arguments.logging:
        logger.set_level_logger(arguments.logging)
        logger.enable_logger(True)

    username = getpass.getuser()

    # Task operations
    if arguments.cmd == 'task':
        if arguments.task_oper == 'add':
            if arguments.interactive:
                tsk = create_task(username)
            else:
                tsk = task.Task(user=username,
                                name=arguments.name,
                                description=arguments.description,
                                time_start=arguments.time_start,
                                time_over=arguments.deadline,
                                priority=arguments.priority,
                                task_groups=arguments.group,
                                delta_period=arguments.regular,
                                parent_id=arguments.parent)

            task_manager.add_task(tsk)

        if arguments.task_oper == 'done':
            task_manager.make_done(arguments.id)

        if arguments.task_oper == 'remove':
            task_manager.remove_task(arguments.id)

        if arguments.task_oper == 'edit':
            edit_dict = {arguments.field: arguments.value}
            task_manager.edit_task(arguments.id, **edit_dict)

        if arguments.task_oper == 'find':
            if arguments.id:
                founded = task_storage.get_task_by_id(arguments.id)
                show_task_short(founded)
            else:
                founded_list = task_storage.get_tasks_by(
                    user=username,
                    name=arguments.name,
                    time_start=arguments.time_start,
                    time_over=arguments.deadline,
                    time_create=arguments.create,
                    group=arguments.group,
                    priority=arguments.priority,
                    status=arguments.status
                )

                for found in founded_list:
                    show_task_short(found)

        if arguments.task_oper == 'show':
            if arguments.choice == 'active':
                for tsk in tasks:
                    if tsk.status == task.ACTIVE:
                        show_task(tsk)

            elif arguments.choice == 'all':
                print_tasks(tasks)

            elif arguments.choice == 'connections':
                connections = task_manager.get_connected_tasks()
                show_connections(connections)

            else:
                if arguments.id:
                    show_task(task_manager.get_task_tree_by_id(arguments.id))
                else:
                    filtered_tasks = task_storage.get_tasks_by(user=username,
                                                               name=arguments.name,
                                                               priority=arguments.priority,
                                                               group=arguments.group,
                                                               status=arguments.status)
                    print_tasks(filtered_tasks)

    # Connection operations
    if arguments.cmd == 'connection':
        if arguments.connection_oper == 'add':
            task_manager.add_connection(arguments.id, arguments.second_id, arguments.blocking)

        if arguments.connection_oper == 'remove':
            task_manager.remove_connection(arguments.id, arguments.second_id)

        if arguments.connection_oper == 'find':
            connected_tasks = task_manager.get_connected_tasks()

            for conn in connected_tasks:
                if arguments.id == conn[0].id or arguments.id == conn[1].id:
                    show_connection(conn)


if __name__ == '__main__':
    main()
