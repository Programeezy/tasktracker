from setuptools import setup, find_packages

setup(
    name='tasktracker',
    version='1.0',
    packages=find_packages(),
    entry_points={'console_scripts': ['tasker=tasker:main']},
    scripts=['console_app/tasker.py'],
    test_suite='tests/operations_test.py',
    url='',
    license='',
    author='tim',
    author_email='',
    description=''
)
